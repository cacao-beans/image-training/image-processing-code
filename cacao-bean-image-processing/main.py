from tensorflow.keras.preprocessing.image import ImageDataGenerator #automatically give label to the images
from tensorflow.keras.preprocessing import image
from PIL import Image, ImageTk
from tensorflow.keras.optimizers import RMSprop
import tensorflow as tf
# import matplotlib.pyplot as plt
from matplotlib import pyplot as plt
import cv2
import os
import numpy as np
import tkinter as tk
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
# import matplotlib
# import cv2
# import numpy
# import pillow


img_path = "C:/Users/joule/OneDrive/Desktop/cacao-beans/training/good-beans/good-1.png"
img = image.load_img(img_path)

# plt.imshow(img)
# plt.show()

cv2.imread(img_path)
# cv2.imread(img_path).shape #use .shape method to check the shape of the image


#ready image from directory paths using ImageDataGenerator
train = ImageDataGenerator(rescale = 1/255)
validation = ImageDataGenerator(rescale = 1/255)

# Set up the training dataset generator
train_dataset =  train.flow_from_directory(
    "C:/Users/joule/OneDrive/Desktop/cacao-beans/training",
    target_size=(200, 200),
    batch_size = 3,
    class_mode = "binary"

)

# Set up the validation dataset generator
validation_dataset = validation.flow_from_directory(
    "C:/Users/joule/OneDrive/Desktop/cacao-beans/validation",
    target_size=(200, 200),
    batch_size = 3,
    class_mode = "binary"

)

# train_dataset.class_indices

# train_dataset.classes
# validation_dataset.classes

# Define the model architecture
model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(16, (3, 3), activation="relu", input_shape=(200, 200, 3)),
    tf.keras.layers.MaxPool2D(2, 2),

    tf.keras.layers.Conv2D(32, (3, 3), activation="relu"),
    tf.keras.layers.MaxPool2D(2, 2),

    tf.keras.layers.Conv2D(64, (3, 3), activation="relu"),
    tf.keras.layers.MaxPool2D(2, 2),

    tf.keras.layers.Flatten(),

    tf.keras.layers.Dense(512, activation="relu"),

    tf.keras.layers.Dense(1, activation="sigmoid")

])

#model compiling
model.compile(
    loss="binary_crossentropy",
    optimizer = RMSprop(learning_rate=0.001),
    metrics = ["accuracy"]
)

#model fitting
model_fit = model.fit(
    train_dataset,
    steps_per_epoch = 3,
    epochs = 30,
    validation_data = validation_dataset
)

dir_path = "C:/Users/joule/OneDrive/Desktop/cacao-beans/testing"

window = tk.Tk()

# Set to keep track of processed file names
processed_files = set()


# Function to process images and display the file name with the final result
def process_images():
    window.update()  # Update the Tkinter window
    window.update_idletasks()  # Process any pending events

    for i in os.listdir(dir_path):
        if i not in processed_files:  # Check if the file is already processed
            img = image.load_img(dir_path + "//" + i, target_size=(200, 200, 3))
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            images = np.vstack([x])
            val = model.predict(images)

            # Determine the file name
            file_name = i

            # Determine the result
            if val == 0:
                result = "bad beans"
            else:
                result = "good beans"

            # Display the image, file name, and result in the Tkinter window
            # img_tk = ImageTk.PhotoImage(img)
            # label = tk.Label(window, image=img_tk)
            # label.image = img_tk  # Keep a reference to the image to prevent it from being garbage collected
            # label.pack()

            # Display the file name and result in a Tkinter label
            label_text = f"{file_name} - {result}"
            result_label = tk.Label(window, text=label_text)
            result_label.pack()

            # Add the processed file name to the set
            processed_files.add(i)

# Function to refresh the program when a new file is added
def refresh_program():
    process_images()

# Watchdog event handler
class CustomEventHandler(FileSystemEventHandler):
    def on_created(self, event):
        refresh_program()

# Watchdog observer
observer = Observer()
event_handler = CustomEventHandler()
observer.schedule(event_handler, dir_path, recursive=False)
observer.start()

# Call the function to process images
process_images()

# Start the Tkinter event loop
window.mainloop()

# Stop the observer when the Tkinter window is closed
observer.stop()
observer.join()